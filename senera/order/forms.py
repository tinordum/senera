from django import forms
from order.models import OrderModel

class OrderForm(forms.ModelForm):

    class Meta:
        model = OrderModel
        fields = ['first_name', 'last_name', 'number_phone', 'region', 'city', 'street', 'house_number',
                  'housing_number', 'apartament_number', 'first_name_sender', 'email_sender', 'phone_sender']
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control',
                                                'id': 'inputFirstName', 'placeholder': 'Имя получателя'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control',
                                                'id': 'inputLastName', 'placeholder': 'Фамилия'}),
            'number_phone': forms.TextInput(attrs={'class': 'form-control',
                                                'id': 'inputNumberPhone', 'placeholder': 'Номер телефона'}),
            'region': forms.TextInput(attrs={'class': 'form-control',
                                                'id': 'inputRegion', 'placeholder': 'Область'}),
            'city': forms.TextInput(attrs={'class': 'form-control',
                                                'id': 'inputCity', 'placeholder': 'Город'}),
            'street': forms.TextInput(attrs={'class': 'form-control',
                                                'id': 'inputStreet', 'placeholder': 'Улица'}),
            'house_number': forms.TextInput(attrs={'class': 'form-control',
                                                'id': 'inputHouseNumber', 'placeholder': 'Дом'}),
            'housing_number': forms.TextInput(attrs={'class': 'form-control',
                                                'id': 'inputHousingNumber', 'placeholder': 'Корпус'}),
            'apartament_number': forms.TextInput(attrs={'class': 'form-control',
                                                'id': 'inputApartamentNumber', 'placeholder': 'Квартира'}),
            'first_name_sender': forms.TextInput(attrs={'class': 'form-control',
                                                'id': 'inputFirstNameSender', 'placeholder': 'Имя'}),
            'email_sender': forms.TextInput(attrs={'class': 'form-control',
                                                'id': 'inputEmailSender', 'placeholder': 'email'}),
            'phone_sender': forms.TextInput(attrs={'class': 'form-control',
                                                'id': 'inputPhoneSender', 'placeholder': 'Номер телефона'})
        }

