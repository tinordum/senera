# Generated by Django 2.0.6 on 2018-08-02 12:08

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0004_ordermodel_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderitem',
            name='product_name',
            field=models.CharField(max_length=200, verbose_name='Товар'),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='quantity',
            field=models.PositiveIntegerField(verbose_name='Количество'),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='total_price',
            field=models.PositiveIntegerField(verbose_name='Общая цена'),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='type',
            field=models.CharField(max_length=200, verbose_name='Тип'),
        ),
        migrations.AlterField(
            model_name='ordermodel',
            name='apartament_number',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='Квартира'),
        ),
        migrations.AlterField(
            model_name='ordermodel',
            name='city',
            field=models.CharField(max_length=100, verbose_name='Город'),
        ),
        migrations.AlterField(
            model_name='ordermodel',
            name='email_sender',
            field=models.EmailField(max_length=254, verbose_name='Email отправителя'),
        ),
        migrations.AlterField(
            model_name='ordermodel',
            name='first_name',
            field=models.CharField(max_length=100, verbose_name='Имя получателя'),
        ),
        migrations.AlterField(
            model_name='ordermodel',
            name='first_name_sender',
            field=models.CharField(max_length=100, verbose_name='Имя отправителя'),
        ),
        migrations.AlterField(
            model_name='ordermodel',
            name='house_number',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='Дом'),
        ),
        migrations.AlterField(
            model_name='ordermodel',
            name='housing_number',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='Корпус'),
        ),
        migrations.AlterField(
            model_name='ordermodel',
            name='last_name',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Фамилия получателя'),
        ),
        migrations.AlterField(
            model_name='ordermodel',
            name='number_phone',
            field=models.CharField(max_length=10, validators=[django.core.validators.RegexValidator(message="Номер должен быть в формате '9633609225'", regex='^\\+?1?\\d{10}$')], verbose_name='Номер телефона получателя'),
        ),
        migrations.AlterField(
            model_name='ordermodel',
            name='phone_sender',
            field=models.CharField(max_length=10, validators=[django.core.validators.RegexValidator(message="Номер должен быть в формате '9633609225'", regex='^\\+?1?\\d{10}$')], verbose_name='Телефон отправителя'),
        ),
        migrations.AlterField(
            model_name='ordermodel',
            name='region',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Регион'),
        ),
        migrations.AlterField(
            model_name='ordermodel',
            name='status',
            field=models.BooleanField(verbose_name='Статус заказа'),
        ),
        migrations.AlterField(
            model_name='ordermodel',
            name='street',
            field=models.CharField(max_length=100, verbose_name='Улица'),
        ),
    ]
