from order.forms import OrderForm
from order.models import OrderItem, OrderModel
from cart.cart import Cart

from django.views.generic.edit import FormView, View
from django.views.generic.base import TemplateView
from django.http import HttpResponseRedirect, HttpResponse
from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User

import weasyprint


class CartOrder(FormView):
    template_name = 'cart/cart.html'
    form_class = OrderForm
    success_url = '/'

    # Этот запрос принимает заполненную форму, отправляет в БД и генерирует PDF
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        order = {}
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            cart = Cart(self.request)
            for i in cart:
                items = OrderItem.objects.create(order_id=post.id, product_name=i.product.name, quantity=i.quantity,
                                         type=i.type, total_price=i.total_price)
            return HttpResponseRedirect('/')
        else:
            return HttpResponse('Введите корректные данные!!!')


class OrderPdf(View):

    def get(self, request, order_id):
        order = get_object_or_404(OrderModel, id=order_id)
        result = 0
        for item in order.items.all():
            result += item.total_price
        html = render_to_string('cart/order.html', {'order': order, 'result': result})
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'filename=order_{}.pdf'.format(order.id)
        weasyprint.HTML(string=html).write_pdf(response,
                                               stylesheets=[
                                                   weasyprint.CSS('static/vendor/bootstrap/css/'
                                                                  'bootstrap.min.css')])
        return response

class Payment(TemplateView):
    template_name = 'cart/payment.html'

    def get_context_data(self):
        context = super(Payment, self).get_context_data()
        cart = Cart(self.request)
        result = 0
        for item in cart:
            result += item.total_price
        count = 0
        string = ''
        for i in cart:
            general = '&LMI_SHOPPINGCART.ITEMS[{}]'.format(count)
            string += general + '.NAME={}'.format(i.product.name)
            string += general + '.QTY={}'.format(i.quantity)
            string += general + '.PRICE={}'.format(i.price)
            string += general + '.TAX=vat0'
            count += 1
        context['string'] = string
        print(string)
        context['result'] = result
        return context


