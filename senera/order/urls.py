from django.urls import path
from order import views
from django.contrib.admin.views.decorators import staff_member_required

app_name = 'order'

urlpatterns = [
    path('', views.CartOrder.as_view(), name="Order"),
    path('pdf/<slug:order_id>/', staff_member_required(views.OrderPdf.as_view()), name="OrderPdf"),
    path('payment/', views.Payment.as_view(), name="Payment")
]