from django import template

register = template.Library()

@register.filter(name='pass_value')
def pass_value(value):
    if value is None:
        return ''
    else:
        return value