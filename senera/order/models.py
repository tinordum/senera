from django.db import models
from django.core.validators import RegexValidator
from product.models import Product

class OrderModel(models.Model):
    phone_regex = RegexValidator(regex=r'^\+?1?\d{10}$', message="Номер должен быть в формате '9633609225'")
    first_name = models.CharField(max_length=100, verbose_name='Имя получателя')
    last_name = models.CharField(max_length=100, blank=True, null=True, verbose_name='Фамилия получателя')
    number_phone = models.CharField(validators=[phone_regex], max_length=10, verbose_name='Номер телефона получателя')
    region = models.CharField(max_length=100, blank=True, null=True, verbose_name='Регион')
    city = models.CharField(max_length=100, verbose_name='Город')
    street = models.CharField(max_length=100, verbose_name='Улица')
    house_number = models.PositiveIntegerField(blank=True, null=True, verbose_name='Дом')
    housing_number = models.PositiveIntegerField(blank=True, null=True, verbose_name='Корпус')
    apartament_number = models.PositiveIntegerField(blank=True, null=True, verbose_name='Квартира')
    first_name_sender = models.CharField(max_length=100, verbose_name='Имя отправителя')
    email_sender = models.EmailField(verbose_name='Email отправителя')
    phone_sender = models.CharField(validators=[phone_regex], max_length=10, verbose_name='Телефон отправителя')
    status = models.BooleanField(default=False, verbose_name='Статус заказа')

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

class OrderItem(models.Model):
    order = models.ForeignKey(OrderModel, related_name='items', on_delete=models.CASCADE)
    product_name = models.CharField(max_length=200, verbose_name='Товар')
    quantity = models.PositiveIntegerField(verbose_name='Количество')
    type = models.CharField(max_length=200, verbose_name='Тип')
    total_price = models.PositiveIntegerField(verbose_name='Общая цена')

    def __str__(self):
        return 'Продукт'

    class Meta:
        verbose_name = 'Предмет заказа'
        verbose_name_plural = 'Предметы заказов'