from django.urls import path
from . import views

app_name = 'product'

urlpatterns = [
    path('<slug:product_slug>/', views.ViewPageProduct.as_view(), name="ViewPageProduct"),
    path('tags/<slug:tag_slug>/', views.ViewTagPage.as_view(), name="PageFilterByTag"),
    path('', views.ViewTagPage.as_view(), name="AllProducts")
]