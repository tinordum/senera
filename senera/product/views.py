from .models import Product
from django.views import generic
from product.models import Category, Product, Tags, ProductPhoto
from django.shortcuts import get_object_or_404
from django.views import generic

class ViewPageProduct(generic.DetailView):
    model = Product
    template_name = 'pages/page_product.html'
    context_object_name = 'product'
    slug_url_kwarg = 'product_slug'

class ViewMainPage(generic.ListView):
    model = Product
    template_name = 'pages/main.html'
    context_object_name = "products"

    def get_context_data(self, **kwargs):
        context = super(ViewMainPage, self).get_context_data(**kwargs)
        context['tags'] = Tags.objects.all
        context['categories'] = Category.objects.all
        context['photos'] = ProductPhoto.objects.all
        return context

# В зависимости от передаваемых параметров возвращает либо продукты сортированные по тегу, либо страницу всех продуктов
class ViewTagPage(generic.ListView):
    model = Product
    template_name = 'pages/page_filter_by_tag.html'
    context_object_name = 'products'
    def get_queryset(self):
        queryset = super(ViewTagPage, self).get_queryset()
        tag = self.kwargs.get('tag_slug')
        if tag:
            self.tag = get_object_or_404(Tags, slug=self.kwargs['tag_slug'])
            return Product.objects.filter(tags=self.tag)
        else:
            return Product.objects.all

    def get_context_data(self, **kwargs):
        context = super(ViewTagPage, self).get_context_data(**kwargs)
        context['tags'] = Tags.objects.all
        return context