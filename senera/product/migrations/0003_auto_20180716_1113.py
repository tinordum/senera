# Generated by Django 2.0.6 on 2018-07-16 11:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0002_auto_20180712_1437'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductPhotoThumbnail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('alt', models.CharField(db_index=True, max_length=200, verbose_name='Альтернативный текст (для роботов) ')),
                ('photo', models.ImageField(upload_to='products/thumbnail/%Y/%m/%d/')),
            ],
        ),
        migrations.AlterField(
            model_name='productphoto',
            name='photo',
            field=models.ImageField(upload_to='products/original/%Y/%m/%d/'),
        ),
    ]
