from django.db import models
from django.contrib.postgres.fields import JSONField
from django.urls import reverse
from PIL import Image
from io import BytesIO


class OptionGroup(models.Model):
    name = models.CharField(max_length=64, verbose_name="Опциональная группа")

    def __str__(self):
        return self.name

class OptionValue(models.Model):
    option_group = models.ForeignKey(OptionGroup, on_delete=models.CASCADE, related_name='values')
    name = models.CharField(max_length=64, verbose_name="Имя опции")

    class Meta:
        ordering = ['name']

class Tags(models.Model):
    name = models.CharField(max_length=200, db_index=True, verbose_name="Название тега")
    slug = models.SlugField(max_length=200, db_index=True, unique=True)

    class Meta:
        ordering = ['name']
        verbose_name = 'Тег'
        verbose_name_plural = 'Теги'

    def get_absolute_url(self):
        return reverse('product:PageFilterByTag', args=[self.slug])

    def __str__(self):
        return self.name

# Модель категории
class Category(models.Model):
    name = models.CharField(max_length=200, db_index=True, verbose_name="Название категории")
    slug = models.SlugField(max_length=200, db_index=True, unique=True)

    class Meta:
        ordering = ['name']
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


# Модель продукта
class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.PROTECT, verbose_name="Категория")
    tags = models.ManyToManyField(Tags)
    name = models.CharField(max_length=200, db_index=True, verbose_name="Название")
    slug = models.SlugField(max_length=200, db_index=True, verbose_name="Slug")
    description = models.TextField(blank=True, verbose_name="Описание")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Создан")
    updated = models.DateTimeField(auto_now=True, verbose_name="Обновлен")

    options = JSONField(blank=True, default={})

    class Meta:
        ordering = ['name']
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
        index_together = [
            ['id', 'slug']
        ]

    def get_absolute_url(self):
        return reverse('product:ViewPageProduct', args=[self.slug])

    def __str__(self):
        return self.name

class ProductPhoto(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='photos')
    alt = models.CharField(max_length=200, db_index=True, verbose_name="Альтернативный текст (для роботов) ")
    photo = models.ImageField(upload_to='products/original/%Y/%m/%d/')

    def image_tag(self):
        return "<img src='{}' alt='{}' class='img-responsive' />".format(self.photo.url, self.alt)

    image_tag.short_description = 'Image'
    image_tag.allow_tags = True

    def __str__(self):
        return 'Photo for "%s"' % self.product.name

class ProductPhotoThumbnail(models.Model):
    product_thumbnail = models.OneToOneField(Product, on_delete=models.CASCADE, related_name='photos_thumbnail')
    alt_thumbnail = models.CharField(max_length=200, db_index=True, verbose_name="Альтернативный текст (для роботов) ")
    photo_thumbnail = models.ImageField(upload_to='products/thumbnail/%Y/%m/%d/')

    def image_tag(self):
        return "<img src='{}' alt='{}' class='img-responsive' />".format(self.photo_thumbnail.url, self.product_thumbnail)

    def __str__(self):
        return 'Photo for "%s"' % self.product_thumbnail.name

    def save(self, *args, **kwargs):
        image_field = self.photo_thumbnail
        image_file = BytesIO(image_field.read())
        image = Image.open(image_file)

        image = image.resize((250, 250), Image.ANTIALIAS)

        image_file = BytesIO()
        image.save(image_file, 'JPEG', quality=90)

        image_field.file = image_file
        super(ProductPhotoThumbnail, self).save()

