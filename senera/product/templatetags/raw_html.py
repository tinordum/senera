from django import template

register = template.Library()

@register.filter(name='raw_html', is_safe=True)
def raw_html(value):
    return value

@register.filter(name='filter_photo')
def filter_photo(image, arg):
    print(image)
    print(arg)
    return image