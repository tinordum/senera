from . import models

class Sale():


    def sale(self, product, price):
        sale_price = product.sale_product.all()[0].value
        if sale_price < 100:
            price = price - (price/100*sale_price)
        else:
            price = price - sale_price
        return price