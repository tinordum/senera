from django.db import models
from product.models import Product
# Create your models here.

class Choice(models.Model):
    name = models.CharField(max_length=20, default="Choice name", verbose_name="Имя")
    value = models.CharField(max_length=50, default="Choice value", verbose_name="Значение")

    def __str__(self):
        return self.name

class SaleProduct(models.Model):
    bouquets = models.ManyToManyField(Product, related_name='sale_product')
    value = models.PositiveIntegerField(verbose_name="Значение")
    choice = models.ForeignKey(Choice, on_delete=models.CASCADE)