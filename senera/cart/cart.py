import datetime
from cart import models

CART_ID = 'CART-ID'

# Здесь заключена основная логика работы корзины при создании, удалении и добавлении товаров.

class ItemAlreadyExists(Exception):
    pass


class ItemDoesNotExist(Exception):
    pass

class Cart:
    def __init__(self, request):
        cart_id = request.session.get(CART_ID)
        if cart_id:
            try:
                cart = models.Cart.objects.get(id=cart_id, checked_out=False)
            except models.Cart.DoesNotExist:
                cart = self.new(request)
        else:
            cart = self.new(request)
        self.cart = cart

    def __iter__(self):
        for item in self.cart.item_set.all():
            yield item

    def new(self, request):
        cart = models.Cart(creation_date=datetime.datetime.now())
        cart.save()
        request.session[CART_ID] = cart.id
        return cart

    # Здесь извлекается цена из json
    def find_price(self, product, type):
        for value in product.options['options'][0]['values']:
            if value['value'] == type:
                return value['price_corrector']

    # Добавление товара в корзину с учетом его типа
    def add(self, product, price, type, quantity=1):
        try:
            item = models.Item.objects.get(
                cart=self.cart,
                product=product,
                type=type,
            )
        except models.Item.DoesNotExist:
            item = models.Item()
            item.cart = self.cart
            item.product = product
            item.price = price
            item.type = type
            item.quantity = quantity
            item.save()
        else:
            item.price = price
            item.type = type
            item.quantity += int(quantity)
            item.save()

    def remove(self, product, type):
        try:
            item = models.Item.objects.get(
                cart=self.cart,
                product=product,
                type=type,
            )
        except models.Item.DoesNotExist:
            raise ItemDoesNotExist
        else:
            item.delete()

    def update(self, product, type, price=None, quantity=1):
        try:
            item = models.Item.objects.get(
                cart=self.cart,
                product=product,
                type=type
            )
        except models.Item.DoesNotExist:
            raise ItemDoesNotExist
        else:
            if quantity == 0:
                item.delete()
            else:
                item.price = price
                item.type = type
                item.quantity = int(quantity)
                item.save()

    def count(self):
        result = 0
        for item in self.cart.item_set.all():
            result += 1 * item.quantity
        return result

    def summary(self):
        result = 0
        for item in self.cart.item_set.all():
            result += item.total_price
        return result

    def clear(self):
        for item in self.cart.item_set.all():
            item.delete()