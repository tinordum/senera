from django.urls import path
from cart import views

app_name = 'cart'

urlpatterns = [
    path('remove/<slug:product_id>/<slug:type>/', views.remove_from_cart, name="CartRemove"),
    path('add/<slug:product_id>/<slug:type>/', views.add_to_cart, name="CartAdd"),
    path('', views.CartOrder.as_view(), name="Cart"),
]
