from product.models import Category, Product, Tags, ProductPhoto
from django.shortcuts import render_to_response, redirect, render
from .cart import Cart
from django.views.generic import FormView
from order.forms import OrderForm
from django.http import HttpResponseRedirect, HttpResponse
from order.models import OrderItem
from sale.sale import Sale


def add_to_cart(request, product_id, type):
    product = Product.objects.get(id=product_id)
    cart = Cart(request)
    price = cart.find_price(product, type)
    if product.sale_product.count():
        price = Sale().sale(product=product, price=price)

    cart.add(product, price=price, quantity=1, type=type)
    total_summ = cart.summary()
    return redirect('/cart', {'cart': Cart(request), 'total': total_summ})

def remove_from_cart(request, product_id, type):
    product = Product.objects.get(id=product_id)
    cart = Cart(request)
    cart.remove(product, type)
    return HttpResponseRedirect('/')

class CartOrder(FormView):
    template_name = 'cart/cart.html'
    form_class = OrderForm
    success_url = '/'

    def form_valid(self, form):
        form.save()
        return super(CartOrder, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CartOrder, self).get_context_data(**kwargs)
        context['cart'] = Cart(self.request)
        context['total_summ'] = Cart(self.request).summary()
        return context