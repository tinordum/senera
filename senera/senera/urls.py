from django.contrib import admin
from django.urls import path, include

from product import urls as product_urls
from cart import urls as cart_urls
from user import urls as user_urls
from order import urls as order_urls
from product.views import ViewMainPage

from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from . import settings

app_name = 'senera'

urlpatterns = [
    path('', ViewMainPage.as_view(), name="MainPage"),
    path('admin/', admin.site.urls),
    path('bouqets/', include(product_urls)),
    path('cart/', include(cart_urls)),
    path('user/', include(user_urls)),
    path('order/', include(order_urls)),
    path('account/', include('django.contrib.auth.urls'))
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()