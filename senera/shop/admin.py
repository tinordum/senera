from django.contrib import admin
from product.models import Tags, Category, Product, ProductPhoto, OptionValue, OptionGroup, ProductPhotoThumbnail
from cart.models import Cart, Item
from django_admin_json_editor import JSONEditorWidget
from product.schema import get_product_option_json_editor_schema

from order.models import OrderItem, OrderModel
from sale.models import SaleProduct, Choice

from django.utils.html import format_html

admin.site.register(Choice)
admin.site.register(SaleProduct)
admin.site.register(Cart)
admin.site.register(Item)
from django.urls import reverse

class OrderItemInline(admin.TabularInline):
    model = OrderItem

@admin.register(OrderModel)
class OrderAdmin(admin.ModelAdmin):
    inlines = [OrderItemInline]
    list_display = ('id', 'first_name', 'last_name', 'number_phone', 'city', 'email_sender', 'OrderPDF')

    def OrderPDF(self, obj):
        return format_html('<a href="{}">PDF</a>'.format(
            reverse('order:OrderPdf', args=[obj.id])
        ))

    OrderPDF.short_description = 'В PDF'

class OptionValueInline(admin.StackedInline):
    model = OptionValue
    extra = 1


@admin.register(OptionGroup)
class OptionGroupAdmin(admin.ModelAdmin):
    inlines = [OptionValueInline]
    actions = None

class PhotoThumbnailInline(admin.StackedInline):
    fields = ('alt_thumbnail', 'image_tag', 'photo_thumbnail',)
    readonly_fields = ('image_tag',)
    model = ProductPhotoThumbnail
    max_num = 1

class PhotoInline(admin.StackedInline):
    fields = ('alt', 'image_tag', 'photo',)
    readonly_fields = ('image_tag',)
    model = ProductPhoto
    extra = 1
    max_num = 5

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name', )}

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    inlines = [PhotoInline, PhotoThumbnailInline]
    prepopulated_fields = {'slug': ('name',)}

    def get_form(self, request, obj=None, **kwargs):
        widget = JSONEditorWidget(get_product_option_json_editor_schema, collapsed=False)
        form = super().get_form(request, obj, widgets={'options': widget}, **kwargs)
        return form

@admin.register(Tags)
class TagAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name', )}





