from django.urls import path
from user import views

app_name = 'user'

APPEND_SLASH = True
urlpatterns = [
    path('registration/', views.RegisterFormView.as_view()),
    path('dashboard/', views.my_view)
]
