from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response

class RegisterFormView(FormView):
    form_class = UserCreationForm
    success_url = "/account/login"
    template_name = "registration/register.html"

    def form_valid(self, form):
        form.save()
        return super(RegisterFormView, self).form_valid(form)

@login_required(redirect_field_name='/')
def my_view(request):
    a = request.user
    print(a)
    return render_to_response('user/dashboard.html', { "a":a } )